/*Программа, собирающая статистические данные о текстовом файле*/
#include <stdio.h>
#include <stdlib.h>

struct SYMB
{
	char ch;
	int count;
};

int comp(const void* a, const void* b)
{
        return ((struct SYMB*)b)->count - ((struct SYMB*)a)->count;
}


int main()
{
	FILE *file;
	char cur, pre = '\0';
	int symbols, words, marks, words_symb, numbers, pop, i, total;
	struct SYMB chars[256];
	symbols = words = marks = words_symb = numbers = pop = total = 0;
	file = fopen("test.txt", "r");

	while((cur = fgetc(file)) != EOF)	
	{
		if(((pre >= 'a' && pre <= 'z') || (pre >= 'A' && pre <= 'Z')) && (cur < 'A' || cur > 'z'))
			words++;

		if(cur != '\n')
		{
			for(i = 0; i < total; i++)
			{
				if(chars[i].ch == cur)
				{
					chars[i].count++;
					break;
				}
			}
			if(i == total)
			{
				chars[total++].ch = cur;
				chars[total].count = 1;
			}
			symbols++;
		}

		if((cur >= 'a' && cur <= 'z') || (cur >= 'A' && cur <= 'Z'))
			words_symb++;

		if(cur >= '0' && cur <= '9')
			numbers++;

		if((cur == '!') || (cur == '?') || (cur == '"') || (cur == '\'') || (cur == '.') || (cur == ',') || (cur == ':')
		 || (cur == ';') || (cur == '-') || (cur == '(') || (cur == ')'))
			marks++;

		pre = cur;	
	}
	
	qsort(chars, total, sizeof(struct SYMB), comp);

	printf("Amount of symbols: %d\n", symbols);
	printf("Amount of words: %d\n", words);
	printf("Amount of marks: %d\n", marks);
	printf("Amount of numbers: %d\n", numbers);

	if(symbols == 0)
	{
		printf("Average length of words: 0\n");	
	}
	else
		printf("Average length of words: %d\n", words_symb/words);

	if(symbols == 0)
	{
		printf("Very popular symbol: \n");
	}
	else if(chars[0].ch != ' ')
	{
		printf("Very popular symbol: %c\n", chars[0].ch);
	}
	else
		printf("Very popular symbol: %c\n", chars[1].ch);
	
	fclose(file);
	return 0;
}
