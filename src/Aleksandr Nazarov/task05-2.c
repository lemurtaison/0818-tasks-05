#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>

int main() {
	char filename[256];
	FILE *file;
	char buff[256];
	char lastsymbol = ' ', mostpopularID;
	int mostpopular[256];
	for (int i = 0; i < 256; i++)
		mostpopular[i] = 0;
	long int symbols = 0, words = 0, marks = 0, num = 0, wordline = 0, mostpopularD = 0;
	do {
		printf("write filename: ");
		fgets(&filename, 256, stdin);
		filename[strlen(filename) - 1] = '\0';
		file = fopen(&filename, "rt");
		if (file == NULL) {            // проверяем существование файла
			printf("wrong filename \n");
		}
	} while (file == NULL);
	while (fgets(buff,256,file)) {
		for (int i = 0; i<(strlen(buff)-1); i++) {
			if (((lastsymbol == ' ') || (lastsymbol == '\n') || (lastsymbol == '\0') || (lastsymbol == '.') || (lastsymbol == ',') || (lastsymbol == ':') || (lastsymbol == ';')) && ((buff[i] >= 'A' && buff[i] <= 'Z') || (buff[i] >= 'a' && buff[i] <= 'z') || (buff[i] >= '0' && buff[i] <= '9'))) {
				words++;
				wordline++;
			}
			else if (((buff[i] >= 'A' && buff[i] <= 'Z') || (buff[i] >= 'a' && buff[i] <= 'z') || (buff[i] >= '0' && buff[i] <= '9'))) {
				wordline++;
			}
			if (buff[i] >= '0' && buff[i] <= '9') {
				num++;
			}
			if ((buff[i] == '!') || (buff[i] == ',') || (buff[i] == '.') || (buff[i] == ':') || (buff[i] == ';') || (buff[i] == '?')) {
				marks++;
			}
			mostpopular[buff[i]]++;
			lastsymbol = buff[i];
			symbols++;
		}
		lastsymbol = ' ';
	}
	wordline = wordline / words;
	for (int i = 0; i < 256; i++) {
		if (mostpopularD <= mostpopular[i]) {
			mostpopularD = mostpopular[i];
			mostpopularID = i;
		}
	}
	printf("Symbols: %d \nWords: %d \nMarks: %d \nNumbers: %d \nMidWordLine: %d \nMost popular symbol: %c", symbols, words, marks, num, wordline, mostpopularID);
	_getch();
	return 0;
}