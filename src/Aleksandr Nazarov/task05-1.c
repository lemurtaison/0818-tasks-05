/*��������� �� ����� ������ ��������� BOOK
��������� �����:
name1
author1
year1
name2
author2
year2
name3
author3
year3
...
nameN
authorN
yearN
*/

#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXSTRING 255

struct BOOK {
	char name[MAXSTRING];
	char author[MAXSTRING];
	int year;
};
void infoprint(struct BOOK* books, int size) {
	for (int i = 0; i < size; i++) {
		printf("title: \"%s\" \nauthor: %s \nyear: %d \n",books[i].name,books[i].author,books[i].year);
	}
}
void swap(struct BOOK* books, int a, int b) {
	char strbuff[MAXSTRING];
	int intbuff;
	strcpy(strbuff, books[a].name);
	strcpy(books[a].name, books[b].name);
	strcpy(books[b].name, strbuff);
	strcpy(strbuff, books[a].author);
	strcpy(books[a].author, books[b].author);
	strcpy(books[b].author, strbuff);
	intbuff = books[a].year;
	books[a].year = books[b].year;
	books[b].year = intbuff;
}
int main() {
	struct BOOK *books;
	char filename[256];
	char buff[MAXSTRING];
	FILE *file;
	int youngest_book = 0, youngest_year = 0, eldest_book = 0, eldest_year = 0, filesize, buffyear;
	filesize = 0;
	do {
		printf("write filename: ");
		fgets(&filename, 256, stdin);
		filename[strlen(filename) - 1] = '\0';
		file = fopen(&filename, "rt");
		if (file == NULL) {            // ��������� ������������� �����
			printf("wrong filename \n");
		}
	} while (file == NULL);
	while (fgets(buff, MAXSTRING, file)) // ������ ���������� ����� � �����
		filesize++;
	if (((filesize % 3) != 0) || (filesize == 0)) {            // ���������, ������� �� ����� � �����, ������� �����, � ���� �� ��� ������.
		printf("wrong structure of file");
		exit(0);
	}
	filesize = filesize / 3; // ������, ������� ��� ���� ������� ��������
	freopen(filename,"rt",file);       // ���������� ��������� � ������ �����
	books = (struct BOOK*)calloc(filesize, sizeof(struct BOOK));
	for (int i = 0; i < filesize; i++) {
		fgets(books[i].name, MAXSTRING, file);
		books[i].name[strlen(books[i].name) - 1] = '\0';
		fgets(books[i].author, MAXSTRING, file);
		books[i].author[strlen(books[i].author) - 1] = '\0';
		fgets(buff, MAXSTRING, file);
		books[i].year = atoi(buff);
		if (books[i].year > youngest_year) {  // �������� ���������� ����� ������ � ����� ����� �����
			youngest_year = books[i].year;
			youngest_book = i;
		}
		if ((books[i].year < eldest_year) || (eldest_year == 0)) {
			eldest_year = books[i].year;
			eldest_book = i;
		}
	}
	fclose(file);
	infoprint(books, filesize);
	printf("Eldest book: \ntitle: \"%s\" \nauthor: %s \nyear: %d \n", books[eldest_book].name, books[eldest_book].author, books[eldest_book].year);
	printf("Youngest book: \ntitle: \"%s\" \nauthor: %s \nyear: %d \n", books[youngest_book].name, books[youngest_book].author, books[youngest_book].year);
	for (int i = 0; i < filesize; i++) {
		for (int j = filesize - i - 1; j > 0; j--) {
			if (books[j].author[0] < books[j - 1].author[0]) { // ���������� �� ��������
				swap(books, j, j - 1);
			}
		}
	}
	infoprint(books, filesize);
	_getch();
	return 0;
}