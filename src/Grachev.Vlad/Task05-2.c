#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#define N 1024

int main()
{	
	FILE *fin;
	char str[N];
	int NumCh, NumLt, NumW, NumPM, NumD, AveLng, flag;
	int i;
	char chars[256];
	NumCh = NumLt = NumW = NumPM = NumD = AveLng = flag = 0;
	for (i = 0; i < 256; chars[i++] = 0);
	fin = fopen("input2.txt", "rt");
	while(fgets(str, N, fin))
		for (i = 0; i < strlen(str); chars[str[i++]]++)
			if ((str[i] >= 'a') && (str[i] <= 'z') || (str[i] >= 'A') && (str[i] <= 'Z'))
			{
				NumLt++;
				if (flag == 0)
				{
					flag = 1;
					NumW++;
				}
			}
			else
				flag = 0;
	fclose(fin);
	chars[' '] = chars['\n'] = chars ['\t'] = 0;
	NumPM = chars[44] + chars[']'] + chars['['] + chars[':'] + chars[';'] + chars['.'] + chars[','] + chars['?'] + chars['!'] + chars['('] + chars[')'] + chars['-'] + chars['"'];// 44 - апостроф
	flag = 0;
	for (i = 0; i < 256; i++)
	{
		NumCh+= chars[i];
		if (str[flag] < str[i])
			flag = i;
		if ((i >= '0') && (i <= '9'))
			NumD += chars[i];
	}
	puts(" -----------------------------------");
	printf("|NumbCh: %11d NumbWrd: %6d|\n", NumCh, NumW);
	printf("|NumbPnctMrk: %6d NumbDgt: %6d|\n", NumPM, NumD);
	printf("|AveWrdLn: %9d MstPplrCh: %4c|\n", NumLt/NumW, str[flag]);
	puts(" -----------------------------------");
	return 0;
}