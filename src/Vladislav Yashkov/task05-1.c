#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <locale.h>
#include <stdlib.h>
#include <string.h>
#define MAX_LENGTH 1000
#define READ_AUTHOR_NAME 0
#define READ_TITLE 1
#define READ_YEAR 2
struct BOOK {
	char author_name[100];
	char title[100];
	int year;
};
void Clear(struct BOOK & book) {
	for (int i = 0; i < 100; i++) {
		book.author_name[i] = '\0';
		book.title[i] = '\0';
	}
}
void PrintBookInfo(struct BOOK book) {
	printf("Author: %s, Title \"%s\", %i y.\n", book.author_name, book.title, book.year);
}
void PrintBooksInfo(struct BOOK * books, int num) {
	for (int i = 0; i < num; i++) {
		printf("Author: %s, Title \"%s\", %i y.\n", books[i].author_name, books[i].title, books[i].year);
	}
}
void SortBooks(struct BOOK * books, int num) {
	for (int i = 0; i < num; i++) {
		for (int j = i; j < num; j++) {
			int k = 0;
			while (books[i].author_name[k] >= books[j].author_name[k]) {
				if (books[i].author_name[k] == books[j].author_name[k]) {
					if (k < 99)
						k++;
					else
						break;
				}
				else {
					struct BOOK tmp = books[i];
					books[i] = books[j];
					books[j] = tmp;
				}
			}
		}
	}
}
int ScanBooks(char** books_in, int num, struct BOOK * books) {
	int books_count = 1;
	for (int i = 0; i < num; i++) {
		int j = 0;
		int mode = READ_AUTHOR_NAME;
		int write_pos = 0;
		char year_tmp[100] = { 0 };
		while (books_in[i][j] != '\0' && books_in[i][j] != '\n') {
			if (books_in[i][j] == '/') {
				//printf("%s\n", books[books_count - 1].author_name);
				if (mode >= READ_YEAR) {
					if (sscanf(year_tmp, "%i", &books[books_count - 1].year)) {
						//printf("%i\n", books[books_count - 1].year);
						for (int k = 0; k < 10; k++) {
							year_tmp[k] = 0;
						}
						books_count++;
						books = (struct BOOK*)realloc(books, books_count * sizeof(struct BOOK));
						Clear(books[books_count - 1]);
					}
					break;
				}
				mode++;
				write_pos = 0;
			}
			else {
				if(write_pos < 100)
					switch (mode) {
						case READ_AUTHOR_NAME:
							books[books_count-1].author_name[write_pos] = books_in[i][j];
							break;
						case READ_TITLE:
							books[books_count-1].title[write_pos] = books_in[i][j];
							break;
						case READ_YEAR:
							year_tmp[write_pos] = books_in[i][j];
							break;
					}
				write_pos++;
			}
			j++;
		}
	}
	return --books_count;
}
struct BOOK OldestBook(struct BOOK * books, int num) {
	int oldest = books[0].year;
	int oldest_num = 0;
	for (int i = 0; i < num; i++) {
		if (books[i].year < oldest) {
			oldest = books[i].year;
			oldest_num = i;
		}
	}
	return books[oldest_num]; 
}
struct BOOK NewestBook(struct BOOK * books, int num) {
	int newest = books[0].year;
	int newest_num = 0;
	for (int i = 0; i < num; i++) {
		if (books[i].year > newest) {
			newest = books[i].year;
			newest_num = i;
		}
	}
	return books[newest_num];
}
int main() {
	setlocale(LC_ALL, "Russian");
	FILE* fp = fopen("file1.txt", "r");
	if (fp == NULL) {
		printf("Error path!\n");
		return 1;
	}
	int num = 1;
	char **books_in = (char**)calloc(num, sizeof(char*));
	books_in[num - 1] = (char*)calloc(MAX_LENGTH, sizeof(char));
	while (fgets(books_in[num - 1], MAX_LENGTH, fp)) {
		if (books_in[num - 1][0] == '\0' || books_in[num - 1][0] == '\n')
			break;
		num++;
		books_in = (char**)realloc(books_in, num * sizeof(char*));
		books_in[num - 1] = (char*)calloc(MAX_LENGTH, sizeof(char));
	}
	struct BOOK * books = (struct BOOK*)calloc(num, sizeof(BOOK));
	num = ScanBooks(books_in, num, books);
	printf("Books Info:\n");
	PrintBooksInfo(books, num);
	printf("Oldest book: \n");
	PrintBookInfo(OldestBook(books, num));
	printf("Newest book: \n");
	PrintBookInfo(NewestBook(books, num));
	SortBooks(books, num);
	printf("Sorted Books:\n");
	PrintBooksInfo(books, num);
	return 0;
}