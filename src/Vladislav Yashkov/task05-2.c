#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <locale.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#define MAX_LENGTH 1000
using namespace std;
class FileInfo {
private:
	char **text;
	int row_size;
	int symbols_size;
	int words_size;
	int punct_size;
	int numbers_size;
	int occurrence_sym[256] = { 0 };
	int word_av_len;
	int freq_symbol;
public:
	FileInfo(char **_text, int _row_size) {
		setInfo(_text, _row_size);
		_getSymbolsSize();
		_getWordsSize();
		_getPunctSize();
		_getNumbersSize();
		_getFreqSymbol();
	};
	void setInfo(char **_text, int _row_size) {
		text = _text;
		row_size = _row_size;
	}
	int getRowSize() {
		return row_size;
	}
	void _getSymbolsSize() {
		int symbolSize = 0;
		for (int i = 0; i < getRowSize(); i++) {
			int j = 0;
			while (j < MAX_LENGTH && text[i][j] != '\0') {
				if (text[i][j] > 32) {
					symbolSize++;
					occurrence_sym[text[i][j]]++;
				}
				j++;
			}
		}
		symbols_size = symbolSize;
	}
	int getSymbolsSize() {
		return symbols_size;
	}
	int isLetter(int c) {
		if ((c >= 48 && c <= 57) || (c >= 56 && c <= 90) || (c >= 97 && c <= 122))
			return 1;
		else
			return 0;
	}
	int isPunct(int c) {
		if (c == ',' || c == '.' || c == '!' || c == '?' || 
			c == '?' || c == '�' || c == '�' || c == '?' || 
			c == ':' || c == ';' || c == '(' || c == ')' ||
			c == '-' || c == '\'' || c == '"')
			return 1;
		else
			return 0;
	}
	void _getWordsSize() {
		int wordsSize = 0;
		int word_len = 0;
		int summ_lens = 0;
		for (int i = 0; i < getRowSize(); i++) {
			int j = 0;
			int c;
			while (j < MAX_LENGTH && text[i][j] != '\0') {
				c = text[i][j];
				if (isLetter(c))
					word_len++;
				else {
					if (word_len > 0) {
						wordsSize++;
						summ_lens += word_len;
						word_len = 0;
					}
				}
				j++;
			}
			if (i + 1 == getRowSize() && word_len > 0) {
				wordsSize++;
				summ_lens += word_len;
			}
		}
		word_av_len = summ_lens / wordsSize;
		words_size = wordsSize;
	}
	int getWordsSize() {
		return words_size;
	}
	void _getPunctSize() {
		int punctSize = 0;
		for (int i = 0; i < getRowSize(); i++) {
			int j = 0;
			while (j < MAX_LENGTH && text[i][j] != '\0') {
				if (isPunct(text[i][j]))
					punctSize++;
				j++;
			}
		}
		punct_size = punctSize;
	}
	int getPunctSize() {
		return punct_size;
	}
	void _getNumbersSize() {
		int numbersSize = 0;
		for (int i = 0; i < getRowSize(); i++) {
			int j = 0;
			while (j < MAX_LENGTH && text[i][j] != '\0') {
				if (text[i][j]>=48 && text[i][j]<=57)
					numbersSize++;
				j++;
			}
		}
		numbers_size = numbersSize;
	}
	int getNumbersSize() {
		return numbers_size;
	}
	int getWordAvLen() {
		return  word_av_len;
	}
	void _getFreqSymbol() {
		int max = 0;
		int max_num = 0;
		for (int i = 0; i < 256; i++) {
			if (occurrence_sym[i] > max) {
				max = occurrence_sym[i];
				max_num = i;
			}
		}
		freq_symbol = max_num;
	}
	int getFreqSymbol() {
		return freq_symbol;
	}
	void printFileText() {
		for (int i = 0; i < getRowSize(); i++) {
			int j = 0;
			while (j < MAX_LENGTH && text[i][j] != '\0') {
				printf("%c", text[i][j]);
				j++;
			}
		}
		printf("\n");
	}
	void message() {
		cout << "website: cppstudio.com\ntheme: Classes and Objects in C + +\n";
	}
};
FileInfo getInfo(FILE* fp) {
	int num = 0;
	char **text;
	text = (char**)calloc(num, sizeof(char*));
	do {
		num++;
		text = (char**)realloc(text, num * sizeof(char*));
		text[num - 1] = (char*)calloc(MAX_LENGTH, sizeof(char));
	} while (fgets(text[num - 1], MAX_LENGTH, fp));
	FileInfo new_info(text,num);
	for (int i = 0; i < num; i++)
		free(text[i]);
	free(text);
	return new_info;
}
int main() {
	setlocale(LC_ALL, "Russian");
	FILE* fp = fopen("file2.txt", "r");
	if (fp == NULL) {
		printf("Error path!\n");
		return 1;
	}
	FileInfo inf = getInfo(fp);
	printf("Symbols size: %i\n", inf.getSymbolsSize());
	printf("Words size: %i\n", inf.getWordsSize());
	printf("Punctuation marks size: %i\n", inf.getPunctSize());
	printf("Numbers size: %i\n", inf.getNumbersSize());
	printf("The average length of a word: %i\n", inf.getWordAvLen());
	printf("The most popular symbol: %c\n", inf.getFreqSymbol());
	return 0;
}