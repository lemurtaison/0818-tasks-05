﻿/*
	Написать программу для сбора статистических данных о текстовом файле. На вход программы подается текстовый файл, а на экран выводятся:
	количество символов
	количество слов
	количество знаков препинания
	количество цифр
	средняя длина слова
	самый популярный символ
*/
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <locale.h>
#define FILENAME "input.txt"
#define FILE_MAXSIZE 8192
#define MIN_CHAR_CODE 32 // Мин. код в ASCII того, что будет считаться за символ
#define AMOUNT_OF_CHARS 256 // Кол-во символов в таблице ASCII (с русскими символами и проч.)

int main() {

	int i;
	FILE *f;
	unsigned char str[FILE_MAXSIZE]; // Русские символы будут иметь положительные коды

	int curr_file_len;
	int file_len = 0;
	int punc_marks = 0;
	int digits_am = 0;

	int words_am = 0;
	unsigned short int is_in_word = 0;

	int char_freq[AMOUNT_OF_CHARS];
	int mostpop_symb;
	int max_freq_symbol = MIN_CHAR_CODE;

	setlocale(LC_ALL, "rus");

	for (i = 0; i < AMOUNT_OF_CHARS; i++) {
		char_freq[i] = 0;
	}

	f = fopen(FILENAME, "rt");

	if (f) {

		while (fgets(str, FILE_MAXSIZE, f)) {
			curr_file_len = strlen(str);
			file_len += curr_file_len;

			for (i = 0; i < curr_file_len; i++) {
				if (str[i] < MIN_CHAR_CODE) continue;
				char_freq[str[i]]++;
				/*
					Определение знаков препинания. Указаны промежутки, соответствующие символам,
					не являющимися английскими/русскими буквами и цифрами, а также символами
					@, _, ', из таблицы ASCII
				*/
				if (str[i] <= 47
					|| (str[i] >= 58 && str[i] <= 63)
					|| (str[i] >= 91 && str[i] <= 94)
					|| (str[i] >= 123 && str[i] <= 127)) {
					is_in_word = 0;
					punc_marks++;
				}
				else {
					if (str[i] >= '0' && str[i] <= '9') {
						digits_am++;
					}
					// Следующий блок сработает на начале очередного слова
					if (!is_in_word) {
						words_am++;
						is_in_word = 1;
					}
				}
			} // Конец for

		} // Конец прохождения по файлу

		// Определение самого популярного символа
		for (i = MIN_CHAR_CODE; i < AMOUNT_OF_CHARS; i++) {
			if (char_freq[i] > char_freq[max_freq_symbol]) {
				max_freq_symbol = i;
			}
		}

		printf("Кол-во символов: %d\n", file_len);
		printf("Кол-во слов: %d\n", words_am);
		printf("Кол-во знаков препинания: %d\n", punc_marks);
		printf("Кол-во цифр: %d\n", digits_am);
		printf("Средняя длина слова: %.2f\n",
			(float)(file_len - punc_marks) / (float)words_am);
		printf("Самый популярный символ: '%c' (%d раз)",
			max_freq_symbol, char_freq[max_freq_symbol]);

	}
	else {
		printf("При открытии файла %s произошла ошибка:\n", FILENAME);
		perror("");

		exit(1);
	}

	return 0;
}