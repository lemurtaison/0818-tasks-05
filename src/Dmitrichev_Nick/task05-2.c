/*
���������, ���������� �������������� ������ � ��������� �����
�������� 6 �������, ������� ������� ��� �������
1) ���������� ��������
2) ���������� ����
3) ���������� ������ ����������
4) ���������� ����
5) ������� ����� �����
6) ����� ���������� ������
*/
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
int File_Symb(FILE *File,char *Y_Name)//������� ���������� �������� � ������, �� ������ ������� � ���������
{
	int res=0;
	char c;
	File = fopen(Y_Name, "rt");
	while (!feof(File))
	{
		c = fgetc(File);
		if ((c != '\n') && (c != ' ') && (c != '	'))
			res++;
	}
	return res - 1;
	fclose(File);
	File = fopen(Y_Name, "rt");
}
int File_Word(FILE *File, char *Y_Name)//������� ���������� ���� � ������, ������������� ���� ������ ������ � ���������
{
	int res = 0;
	File = fopen(Y_Name, "rt");
	char c = fgetc(File);
	while (c!=EOF)
	{
		while (((c == ' ')|| (c == '\n')||(c=='\t')) &&(c!=EOF))
			c=fgetc(File);
		if ((c!=' ')&&(c!='\t')&&(c!='\n')&&(c!=EOF))
			res++;
		while (((c != ' ')&&(c!='\t') && (c != '\n')) &&(c!=EOF))
			c=fgetc(File);
	}
	fclose(File);
	File = fopen(Y_Name, "rt");
	return res;
}
int File_Punc(FILE *File, char *Y_Name)//������� ����� ���������� � ������
{
	int res = 0;
	File = fopen(Y_Name, "rt");
	char c = fgetc(File);
	while (c != EOF)
	{
		if ((c == '\'') || (c == '\n') || (c == '-') || (c == '"') || (c == '(') || (c == ')') ||
			(c == '.') || (c == ',') || (c == ':') || (c == ';') || (c == '!') || (c == '?') || (c == ' '))
			res++;
		c = fgetc(File);
	}
	fclose(File);
	File = fopen(Y_Name, "rt");
	return res;
}
int File_Digi(FILE *File, char *Y_Name)//������� ���������� ���� � ������
{
	int res = 0;
	File = fopen(Y_Name, "rt");
	char c = fgetc(File);
	while (c != EOF)
	{
		if ((c>='0')&&(c<='9'))
			res++;
		c = fgetc(File);
	}
	fclose(File);
	File = fopen(Y_Name, "rt");
	return res;
}

int File_Sred(FILE *File, char *Y_Name)//������� ������� ����� ����� (����� �����)
{
	int res;
	res = (int)(File_Symb(File, Y_Name) / File_Word(File, Y_Name));
	return res;
}
char File_Ofte(FILE *File, char *Y_Name)//������� ��� ������ ����������� �������
{
	char res = 0;
	char buf[127];
	File = fopen(Y_Name, "rt");
	char c, i,tmp=0;
	for (i = 0;i < 127;i++)
		buf[i] = 0;
	c = fgetc(File);
	while (c != EOF)
	{
		if ((c!=' ')&& (c != '\n') && (c != '\0') && (c != '\t') )
			buf[c]++;
		c = fgetc(File);
	}
	fclose(File);
	for (i = 0;i < 127;i++)
		if (buf[i] > tmp)
		{
			res = i;
			tmp = buf[i];
		}
	File = fopen(Y_Name, "rt");
	return res;
}
int main()
{
	FILE *File;
	char Y_Name[10],Check=0;
	puts("Enter the name of your file ('name.name' for example)");
	fgets(Y_Name, 9, stdin);
	while ((File = fopen(Y_Name, "rt")) == 0)
	{
		puts("Program hasn't found this file\nTry again\n");
		fgets(Y_Name, 9, stdin);
	}
	Check = fgetc(File);
	if (Check != EOF)
	{
		puts("\nIn your file:\n");
		printf("Symbols: %d\n", File_Symb(File, Y_Name));
		printf("Words: %d\n", File_Word(File, Y_Name));
		printf("Punctuation signs: %d\n", File_Punc(File, Y_Name));
		printf("Number of digits: %d\n", File_Digi(File, Y_Name));
		printf("Average length of the word: %d\n", File_Sred(File, Y_Name));
		printf("The most popular symbol: %c\n", File_Ofte(File, Y_Name));
	}
	else
		puts("Empty file");
	fclose(File);
	return 0;
}