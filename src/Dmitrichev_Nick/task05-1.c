/*
�� ��������������� �������� ����� ������� ���������� � ������ � ������������ ������ �������� �
1)����������� ������ � ������ �� ������
2)����������� ������ � ����� ������ � ����� ����� ����� (�� ���� �������)
3)����������� ������, ��������������� �� �������� �������
*/
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#define Dan 50//����������� �� ����� �������� ����� � ����� �������/����� ������
#define Long_Line "____________________________________________________________________________"//����� ����� ����, ��� ��� �������� �����
typedef struct Book Tr_Book;

struct Book
{
	char Title[Dan];
	char Author[Dan];
	int Year;
};

void Put_Book_Scr(char N,Tr_Book *p)//������� ��� ������ � ����� �� ����������
{
	printf("%-35s	%-25s	%d\n", p[N].Title, p[N].Author, p[N].Year);
}

char Old_Book(char N,Tr_Book *p)//����� ������ �����
{
	N--;
	char res = N;
	while (N > 0)
	{
		if (p[N].Year < p[res].Year)
			res = N;
		N--;
	}
	return res;
}

char Yon_Book(char N, Tr_Book *p)//����� ����� �����
{
	N--;
	char res = N;
	while (N > 0)
	{
		if (p[N].Year > p[res].Year)
			res = N;
		N--;
	}
	return res;
}

char Str_In_F(FILE *Str_F,char *Y_Name)//������� ���������� ����� � ����� (�������� ������ ����� fopen)
{
	char i = 0;
	char buf[256];
	while (!feof(Str_F))
		if (fgets(buf, 256, Str_F))
		i++;
	fclose(Str_F);
	Str_F = fopen(Y_Name, "rt");
	return i;
}

void Get(char *arr, Tr_Book *p)//������ ������ �� ����� � ��������� Book
{
	char i = 0,j=0,x;
	char buf[Dan];
	for ( x = 0;x < 3;x++)
	{
		while ((arr[i] != '	') && (arr[i] != '\n'))
		{
			buf[j++] = arr[i++];
		}
		buf[j] = 0;
		i++;
		j = 0;
		switch (x)
		{
		case 0:
			strcpy(p->Title, buf);
		case 1:
			strcpy(p->Author, buf);
		case 2:
			p->Year = atoi(buf);
		}
	}
}
void Swap(char i, char j, Tr_Book *p)//������ ������� ��� ��������� � �������
{
	Tr_Book tmp=p[i];
	p[i] = p[j];
	p[j] = tmp;
}
void Sort_Lib(char N, Tr_Book *p)//���������� �� ������� � ���������� �������
{
	char i,j;
	N--;
	for (i = 0;i < N;i++)
		for (j = i;j < N;j++)
		{
			if ((strcmp(p[i].Author, p[j].Author))>0)//������������ ������� strcmp
				Swap(i, j, p);
		}
}
int main()
{
	
	Tr_Book *Lib;
	FILE *File;
	char File_Str,i=0;
	char Y_Name[10], buf[2 * Dan + 4];
	puts("Enter the name of your file ('name.name' for example)");
	fgets(Y_Name, 9, stdin);
	while ((File = fopen(Y_Name, "rt")) == 0)
	{
		puts("Program hasn't found this file\nTry again\n");
		fgets(Y_Name, 9, stdin);
	}
	File_Str = Str_In_F(File,Y_Name);
	puts("\nResult:\n");
	printf("%-35s	%-25s	%s\n\n", "Title", "Author", "Year");
	Lib = (Tr_Book*)malloc(File_Str*sizeof(Tr_Book));
	while (i < File_Str)
	{
		fgets(buf, 2*Dan+4, File);
		Get(buf, &(Lib[i]));
		i++;
	}
	for (i = 0;i < File_Str;i++)//������� ����� �� �����
	{
		Put_Book_Scr(i, Lib);
	}
	puts(Long_Line);
	printf("The Oldest book: \n");
	Put_Book_Scr(Old_Book(File_Str, Lib),Lib);
	puts(Long_Line);
	printf("The Youngest book: \n");
	Put_Book_Scr(Yon_Book(File_Str, Lib), Lib);
	puts(Long_Line);
	puts("Sorting of authors:\n");
	Sort_Lib(File_Str, Lib);
	for (i = 0;i < File_Str;i++)//����� ��������������� ��������
	{
		Put_Book_Scr(i, Lib);
	}
	puts(Long_Line);
	free(Lib);
	fclose(File);
	return 0;
}